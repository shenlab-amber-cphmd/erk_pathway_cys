import numpy as np
import mdtraj

pdbs = ['araf','6p7g','3omv','7juw','7jur','7b3m','1s9i','6ges','6g54','6xd3','5ACB','7nxj']
labels=['ARAF','BRAF','CRAF','KSR1','KSR2','MEK1','MEK2','ERK1','ERK2','CDK7','CDK12','CDK13']

cys = { 'ARAF': [77,206],
        'BRAF': [532],
        'CRAF': [424],
        'KSR1': [676,689],
        'KSR2': [729,742,830],
        'MEK1': [121,207,376],
        'MEK2': [125,211,384],
        'ERK1': [144,183,271],
        'ERK2': [166,254],
        'CDK7': [312],
        'CDK12':[1039],
        'CDK13': [1017]}

minDists = {}
minDists_ca = {}
#Loop over systems
for pdb, label in zip(pdbs,labels):
    #Load pocket
    if(pdb=='6g54' or pdb=='1s9i' or pdb=='3omv'):    #Main pocket is second identified 
        pocketVerts = np.loadtxt('fpocket_outputs/fixed_{}_out/pockets/pocket2_vert.pqr'.format(pdb),usecols=(5,6,7),comments=['HEADER','TER','END'])
    elif(pdb=='6ges'):
        pocketVerts = np.loadtxt('fpocket_outputs/fixed_{}_out/pockets/pocket4_vert.pqr'.format(pdb),usecols=(5,6,7),comments=['HEADER','TER','END'])
    elif(pdb=='7b3m'):
        pocketVerts = np.loadtxt('fpocket_outputs/fixed_{}_out/pockets/pocket3_vert.pqr'.format(pdb),usecols=(5,6,7),comments=['HEADER','TER','END'])
    elif(pdb=='6p7g'):
        pocketVerts = np.loadtxt('fpocket_outputs/fixed_{}_B_out/pockets/pocket1_vert.pqr'.format(pdb),usecols=(5,6,7),comments=['HEADER','TER','END'])
    elif(pdb=='7nxj'):
        pocketVerts = np.loadtxt('fpocket_outputs/fixed_{}_out/pockets/pocket5_vert.pqr'.format(pdb),usecols=(5,6,7),comments=['HEADER','TER','END'])
    else:
        pocketVerts = np.loadtxt('fpocket_outputs/fixed_{}_out/pockets/pocket1_vert.pqr'.format(pdb),usecols=(5,6,7),comments=['HEADER','TER','END'])
    #Load pdb and find cys
    if(pdb=='6p7g'):    #Use chain B instead of A 
        struct = mdtraj.load('fixedPDBs/fixed_{}_B.pdb'.format(pdb))
    else:
        struct = mdtraj.load('fixedPDBs/fixed_{}.pdb'.format(pdb))
    sele = 'name SG and residue '
    for resid in cys[label]:
        sele+='{} '.format(resid)
    sgAtomsIdx = struct.topology.select(sele[:-1])
    atomLoc = [struct.xyz[0,idx]*10. for idx in sgAtomsIdx]
    #Loop over cys
    for atom,resid in zip(atomLoc,cys[label]):
        #Calculate minimum distance
        dist = np.amin([np.sqrt(np.sum(np.power(atom-vert,2))) for vert in pocketVerts])
        minDists['{}-Cys{}'.format(label,resid)] = dist
    caSele = 'name CA and residue '
    for resid in cys[label]:
        caSele+='{} '.format(resid)
    caAtomIdx = struct.topology.select(caSele[:-1])
    atomLoc = [struct.xyz[0,idx]*10. for idx in caAtomIdx]
    for atom,resid in zip(atomLoc,cys[label]):
        dist = np.amin([np.sqrt(np.sum(np.power(atom-vert,2))) for vert in pocketVerts])
        minDists_ca['{}-Cys{}'.format(label,resid)] = dist


#Write to file
f=open('cysPocketDistance.dat','w')
for key in minDists.keys():
    f.write('{:16s}{:6.2f}{:6.2f}{:6.2f}\n'.format(key,minDists[key],minDists_ca[key],minDists[key]-minDists_ca[key]))
f.close()
