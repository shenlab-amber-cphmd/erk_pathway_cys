#!/bin/bash

# Used to calculate salt bridge occupancy between cysteine and a basic residue

PARM=$1
N=$2
FIRST=$3
LAST=$4

COMMAND="parm $PARM \n"

for i in `seq $FIRST $LAST`
do
    COMMAND="$COMMAND trajin output_${i}/rep${N}.nc 0 200 2 \n"
done
#COMMAND="$COMMAND unwrap center \n"
COMMAND="$COMMAND autoimage \n"

COMMAND="$COMMAND hbond dist 4.0 angle -1 donormask :417@NZ acceptormask :358@SG out C358_K417_sb_${N}.dat avgout C358_K417_avg_${N}.dat \n"


COMMAND="$COMMAND go"
echo -e "$COMMAND" | cpptraj


