#!/usr/bin/env python
## Get the S vs pH fit function from a S_pH data file
## Usage: pKa_fit.py <datafile> <project>
import sys
import glob
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import math
from scipy.optimize import curve_fit
import re

matplotlib.rcParams.update({'errorbar.capsize': 2})
numbers = re.compile(r'(\d+)')
def numericalSort(value):
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts
## parse inputs
pdb = sys.argv[1]
resid = sys.argv[2]
figmat = sys.argv[3]
flag = sys.argv[4]
initial_pka = 3.0
if len(sys.argv) > 5:
    initial_pka = float(sys.argv[5])


## define fit and error equations
def hh_equation_fitting(ph, hill, pka):
        return 1 / (1 + 10**(hill*(pka - ph)))
def hh_equation(parms, ph):
        return 1 / (1 + 10**(parms[0] * (parms[1] - ph)))

font = {'family': 'sans-serif',
        'color': 'black',
        'fontname': 'Liberation Sans',
        'weight': 'normal',
        'size': 14,
}
prop = {'family': 'sans-serif',
        'size': 12,
       }
## read lambda data and write out dU/dtheta vs theta data
S = []
pH = []
std = []
begin = 0
end = 0
colors = ['black', 'blue', 'red', 'orange']
plt.figure(figsize=(3.75, 3.0)) #Uncommen this for publication

results = []
error_flag_init = 1
for i, lfile in enumerate(sorted(glob.glob("{}-{}-{}.data".format(pdb, resid, flag)), key=numericalSort)):
    S.append([])
    std.append([])
    results.append([])
    with open(lfile, 'r') as tmpf:
        for line in tmpf:
            if line[0] != '#':
                tokens = line.split()
                if i == 0:
                    pH.append(float(line.split()[0]))
                S[i].append(float(line.split()[1]))
                if len(line.split()) >= 3 and error_flag_init == 0:
                    error_flag = 1
                    std[i].append(float(line.split()[2]))
                else:
                    error_flag = 0
    ## fit data with leastsq 
    if i == 0:
        initial_parms = [1, initial_pka]
        pHs = pH
        pH = np.array(pH)
        pHa = np.linspace(pH.min(), pH.max(), 100)
    S_tmp = S[i]
    try:
        fit_parms = curve_fit(hh_equation_fitting, pHs, S_tmp, p0 = initial_parms)
        results[i].append(fit_parms[0][0])
        results[i].append(math.sqrt(fit_parms[1][0][0]))
        results[i].append(fit_parms[0][1])
        results[i].append(math.sqrt(fit_parms[1][1][1]))
        if error_flag == 1:
            plt.errorbar(pH, S_tmp, std[i], linestyle='None', fmt='-o', \
                        capthick=2, color=colors[i], label='{}-{} (with mix)'.format(pdb, resid))
        else:
            plt.plot(pH, S_tmp, 'o', color=colors[i], label='{}-{}'.format(pdb, resid))
        final_parms = [fit_parms[0][0], fit_parms[0][1]]
        #plt.plot(pHa, hh_equation(final_parms, pHa), color=colors[i])
        if final_parms[0] > 0.1:
            plt.plot(pHa, hh_equation(final_parms, pHa), color=colors[i], label='Fitted p' \
                    + r'$K_{a}$' + ' = {:.2f}, \n           err = {:.2f}'.format(fit_parms[0][1], results[i][1]))
        else:
            print('Bad fitting results! pKa={:.2f}, Hill_coeff={:.2f} < 0.5.'.format(fit_parms[0][1], fit_parms[0][0]))
    except RuntimeError:
        print('No fitting results for Residue {}!'.format(resid))
        if error_flag == 1:
            plt.errorbar(pH, S_tmp, std[i], linestyle='None', fmt='-o', \
                        capthick=2, color=colors[i], label='{}-{} (with mix)'.format(pdb, resid))
        else:
            plt.plot(pH, S_tmp, 'o', color=colors[i], label='{}-{}'.format(pdb, resid))


plt.xlabel('pH', fontdict=font)
plt.ylabel('Unprot Frac', fontdict=font)
y_range = np.arange(-0.02, 1.02, 0.2)
y_tic = ['{:.1f}'.format(float(i)) for i in y_range]
x_range = np.arange(math.floor(min(pH)), math.floor(max(pH))+1, 1.0)
x_tic = ['{:.1f}'.format(float(i)) for i in x_range]
plt.xticks(x_range, x_tic, fontname=font['fontname'], fontsize=font['size'], fontweight=font['weight'])
plt.yticks(y_range, y_tic, fontname=font['fontname'], fontsize=font['size'], fontweight=font['weight'])
plt.legend()
plt.tight_layout()
plt.savefig(pdb +'-'+  resid + '_titration_{}.'.format(flag) + figmat, bbox_inches='tight', transparent=False) # Change 'transparent' to false for publication
plt.close()
