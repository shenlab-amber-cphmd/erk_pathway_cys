#!/bin/bash

# Script Used to Calculate Hbonding occupancy between reactive cysteine and neighbouring residues
################### Procedure ##################################################################################################
# 1. Initial scan done between target cysteine as and rest of protein using no angle cut-off and a cut-off distance of 4.5 Ang #
# 2. Then proper Analysis done between target cysteine and surrounding residues with high occupancy from previpous calculation #
################################################################################################################################

PARM=$1
N=$2
FIRST=$3
LAST=$4
SEL1=$5
SEL2=$6
OUT=$7

#####################################################################################################################
echo "source /usr/local/lib/vmd/plugins/noarch/tcl/hbonds1.2/hbonds.tcl" >> hbond_run_${N}.tcl
echo "mol new {$PARM.parm7} type {parm7} first 0 last -1 step 1 waitfor 1" >> hbond_run_${N}.tcl
for i in `seq $FIRST $LAST`
do
	echo "mol addfile {output_${i}/rep${N}.nc} type {netcdf} first 0 last -1 step 1 waitfor -1 0" >> hbond_run_${N}.tcl
done
#####################################################################################################################

echo "set sel1 [atomselect top \"resid $SEL1\"]" >> hbond_run_${N}.tcl
echo "set sel2 [atomselect top \"resid $SEL2\"]" >> hbond_run_${N}.tcl
echo "
##########################
# Hbond calculation     ##
##########################
hbonds -sel1 \$sel1 -sel2 \$sel2 -writefile yes -upsel yes -frames all -dist 3.5 -ang 45 -plot no -outdir .  -log $OUT.log -writefile yes -outfile $OUT.dat -polar yes -DA both -type unique -detailout ${OUT}_detail.out
" >> hbond_run_${N}.tcl
echo "exit" >> hbond_run_${N}.tcl

vmd -dispdev none -e hbond_run_${N}.tcl > hbond.log 2>&1
rm *.dat
rm *.log

