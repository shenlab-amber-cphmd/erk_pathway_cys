#!/bin/bash


####################################
# Take care of inputs #
####################################

echo 'Start' > job_status

# Default job settings
me=`basename "$0"`
POSITIONAL=()
PDBID=''
PDBFILE=''
CHAINID='A'
MODELID=''
ispdbfixed=false	# If true, will generate parm7 and rst7 files directly from it.
istest=false          	# If true, tepmorary files are not deleted
isrestart=false       	# If true, restart from the previous production run
buildonly=false		# If true, won't perform heating, equilibration, and production calculaitons
# Parse input arguments
usage()
{
	echo "Usage: $me [[-p|--pdbid PDBID]|[-f|--pdbfile PDBFILE]] [-c|--chainid CHAINID(default: A)] [-m|--modelid MODELID(default: None)] [-fx|--fixed ispdbfixed(default: false] [-t|--test istest(default: false] [-b|--build buildonly(default: false)] [-r|--restart isrestart(default: false)] [-h|--help]"
}
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -p|--pdbid)
    PDBID="$2"
    shift # past argument
    shift # past value
    ;;
    -f|--pdbfile)
    PDBFILE="$2"
    shift # past argument
    shift # past value
    ;;
    -c|--chainid)
    CHAINID="$2"
    shift # past argument
    shift # past value
    ;;
    -m|--modelid)
    MODELID="$2"
    shift # past argument
    shift # past value
    ;;
    -h|--help)
    usage
    exit  
    shift # past value
    ;;
    -t|--test)
    istest=true
    shift # past argument
    ;;
    -r|--restart)
    isrestart=true
    shift # past argument
    ;;    
    -b|--build)
    buildonly=true
    shift # past argument
    ;;
    -fx|--fixed)
    ispdbfixed=true
    shift # past argument
    ;;
    *)    # unknown option
    usage
    exit  
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [ "$PDBID" == '' ] && [ "$PDBFILE" == '' ]
then
	echo "Must provide a PDB id or a PDB file."
	usage
	exit
elif [ "$PDBID" != '' ] && [  "$PDBFILE" == '' ]
then
	echo "PDB ID = ${PDBID}"
	dirname=$PDBID
elif [ "$PDBID" == '' ] && [ "$PDBFILE" != '' ]
then
	PDBFILE=`basename "$PDBFILE"`
	dirname=$(echo ${PDBFILE%.pdb})
	echo "PDB file = ${PDBFILE}"
else
	echo "Must provide either a PDB id or a PDB file, not both."
	usage
	exit
fi

echo "Chain ID = ${CHAINID}"

if [ "$MODELID" != '' ]
then
	echo "Model ID = ${MODELID}"
else
	MODELID='-'
fi

if [ "$istest" = true ]
then
	echo "This is a test job."
fi

if [ "$isrestart" = true ] && [ "$buildonly" = false ]
then
	echo "This is a restart from previous job."
fi

if [ "$buildonly" = true ]
then
	echo "Only Amber parm7 and rst7 (minimized) files will be generated."
fi

echo 'Initiated' > job_status
#######################################
# prepared Amber CpHMD ready PDB file #
#######################################
currentdir=`pwd`

echo "Check: ${currentdir}/$PDBFILE"

if [ "$ispdbfixed" = false ]
then
	py3dir=`/home/akrom/software/anaconda3/bin/python`
	if [ -z $py3dir ]
	then
		echo "Need python3 installed first."
		exit
	else
		[[ -d $dirname ]] || mkdir $dirname
		cd $dirname
		if [ "$PDBID" != '' ]; then
			preparePDB.py -p $PDBID -c $CHAINID -m $MODELID > prepare.log
			fixedpdb=`ls ${PDBID}*_fixed.pdb`
		else
			preparePDB.py -f ${currentdir}/$PDBFILE -c $CHAINID -m $MODELID > prepare.log
			fixedpdb=`ls *_fixed.pdb`
		fi
	fi
	wait
	protein=$(echo ${fixedpdb%.pdb})
else
	[[ -d $dirname ]] || mkdir $dirname
	cd $dirname
	cp ${currentdir}/$PDBFILE .
	protein=$(echo ${PDBFILE%.pdb})
fi

echo "The fixed PDB file is ${protein}.pdb"
echo 'PDB-fixed' > ${currentdir}/job_status
#####################################
# Paths and files for tleap         #
#####################################
leapdir='/home/akrom/software/amber16/bin/tleap'
if [ -z "$leapdir" ]
then
        echo "Install Ambertools first."
        exit 
else
        tmpdir=$(dirname "$(dirname "$leapdir")")
        leapdir=${tmpdir}/dat/leap
fi

inputparm="/home/akrom/projects/kinases/Braf/6p7g/cphmd/input.parm"
######################
# Standard Variables #
######################
conc=0.15    	# Salt Concentration, Implicent Salt Concentration
crysph=7.5      # Starting pH, can come from crystal structure for protein
cutoff=999.0    # Nonbond cutoff small system 9.0 ang, otherwise use 12.0 ang; for GB, use 999.0
ts=0.002	# Time step

########################
# Minimization Options #
########################
pmemddir='/home/akrom/software/amber16/bin/pmemd.cuda'
if [ -z $pmemddir ]
then
	usepmemd=false           # Whether to use pmemd.cuda to minimize
else
	usepmemd=true
fi
np=4			# Number of processors used for energy minimization
nsteps=1000      	# Number of total minimization steps
nsdsteps=500     	# Initial amount of total minimization as SD steps
wfrq=50         	# How often to write ene. vel. temp. restart and lamb.
restraint=50.0		# restraints applied on restrainted atoms

################
# PHMD Options #
################
lfrq=1000         # How often to print lambda values for heating, equil., and prod.
pHs=(6.5 7.0 7.5 8.0 8.5 9.0 9.5 10.0)
###############################################
# Heating NOT Used for Implict Solvent Model  #
###############################################

########################
# Equilibration Options #
########################
etemp=300               # Equilibration temperature
ensteps=2000            # Number of each equilibration step
ewfrq=100               # How often to write ene. vel. temp. and lamb.
ewrst=2000              # How often to write restart 
erestraints=(5.0 2.0 1.0 0.0)          # restraints applied on restrainted atoms
ets=$ts

########################
# Production Options #
########################
pnsteps=500000          # Number of total production steps
pwfrq=5000                # How often to write trajectories.
pwrst=500000            # How often to write restart
pts=$ts

echo 'tleap-begin' > ${currentdir}/job_status
if [ "$isrestart" = false ] ; then
    ################################################
    # Building $protein using TLEaP #
    ################################################
    
    # Make build.in file
    echo "# Reads in the FF
    source $leapdir/cmd/leaprc.protein.ff14SB     			# Load protein
    source $leapdir/cmd/leaprc.water.tip3p        			# Load pre-equilibrated water, not used in GB-CpHMD
    set default PBradii mbondi3                             	# Modifies GB radii to mbondi3 set, needed for IGB8 this is the version of the GB
    loadOff $currentdir/phmd.lib                                	# load modified library file, loads definitions for AS2 GL2 (ASPP AND GLUPP)
    phmdparm = loadamberparams  $currentdir/frcmod.phmd_barrier       # load modifications to bond length and dihedrals, and raises dihe barrier.
    $protein = loadPDB $protein.pdb                            	# Load PDBi
    saveamberparm $protein $protein.parm7 $protein.rst7           	# generates the parameter files and coords file
    savepdb $protein $protein.pdb_tmp                    # save the pdb file with right atom numbering scheme wrt the parm7 file
    quit
    " > build.in
   
    # Run tleap
     /home/akrom/software/amber16/bin/tleap -f build.in > tleap.log
    wait
    echo 'tleap-end' > ${currentdir}/job_status
    
    # Change the radii of SG in Cys, HZ* in LYS, and HD1/HE2 in HIP using processParm7.py
    # Interaction between HD1(HE1) and HD2(HE2) dummy hydrogens in AS2/GL2 is also added to the exclusion list
    $py3dir  /home/akrom/projects/kinases/Braf/6p7g/cphmd/processParm7.py -f $protein.pdb_tmp -p $protein.parm7
    mv $protein.parm7 orig_$protein.parm7
    mv cphmd_$protein.parm7 $protein.parm7
    wait
    mv $protein.pdb_tmp $protein.pdb
    echo 'parm7-prepared' > ${currentdir}/job_status

    #############################
    #  Perform Minimization #
    #############################
    
    # Make $protein_mini.mdin
    nres=$( grep "FLAG POINTERS" -A 3 ${protein}.parm7 | tail -n 1 | awk '{print $2}' )  
    nrestr=$(($nres - 2))
    echo "Minimization
    &cntrl                                         ! cntrl is a name list, it stores all conrol parameters for amber
      imin = 1, maxcyc = $nsteps, ncyc = $nsdsteps, ! Do minimization, max number of steps (Run both SD and Conjugate Gradient), first 250 steps are SD
      ntx = 1,                                     ! Initial coordinates
      ntwe = 0, ntwr = $nsteps, ntpr = $wfrq,        ! Print frq for energy and temp to mden file, write frq for restart trj, print frq for energy to mdout 
      ntc = 1, ntf = 1, ntb = 0, ntp = 0,          ! Shake (1 = No Shake), Force Eval. (1 = complete interaction is calced), Use PBC (1 = const. vol.), Use Const. Press. (0 = no press. scaling)
      cut = $cutoff,                               ! Nonbond cutoff (Ang.)
      ntr = 1, restraintmask = ':3-$nrestr&!@H=',    ! restraint atoms (1 = yes), Which atoms are restrained 
      restraint_wt = $restraint,                   ! Harmonic force to be applied as the restraint
      ioutfm = 1, ntxo = 2,                        ! Fomrat of coor. and vel. trj files, write NetCDF restrt fuile for final coor., vel., and box size
      igb = 8,
    /" > ${protein}_mini.mdin  
    
    # Run minimization with sander/pmemd.cuda
    echo 'minimization-begin' > ${currentdir}/job_status
    $pmemddir -O -i ${protein}_mini.mdin -c ${protein}.rst7 -p ${protein}.parm7 -ref ${protein}.rst7 -r ${protein}_mini.rst7 -o ${protein}_mini.out
    wait
    echo 'minimization-end' > ${currentdir}/job_status
    if [ "$buildonly" = true ]; then
	    echo "Amber parm7 and rst7 files are prepared!"
	    exit
    fi
    
    #############################
    #  Perform Equilibration #
    #############################
    
    # Create the phmdin file for following steps #
    echo "&phmdin
        QMass_PHMD = 10,                    		! Mass of the Lambda Particle
        Temp_PHMD = 300,                    		! Temp of the lambda particle
        phbeta = 5,                         		! Friction Coefficient (1/ps) for titration integrator
        iphfrq = 1,                         		! Frequency to update the lambda forces
        NPrint_PHMD = $lfrq,                		! How often to print the lambda
        PrLam = .true.,                     		! Should lambda be printed
        PrDeriv = .false.,                  		! Do you want to print the derivatives?
        PRNLEV = 7,                         		! Sets what is printed out, (7) normal print level
        PHTest = 0,                         		! Are lambda and theta fixed, (0) = not fixed, (1) = fixed
        MaskTitrRes(:) = 'AS2','GL2','HIP','LYS','CYS', 	! Residues to include as titratable
	MaskTitrResTypes = 5,                           ! number of titratable types
        QPHMDStart = .true.,                		! Initialize velocities of titration varibles with a Boltzmann distribution
    /" > ${protein}_phmdin 
    
    # Create input files for equilibration
    for restn in `seq 1 ${#erestraints[@]}` # loop over number of restarts
    do
	echo "equilibration step $restn begin" > ${currentdir}/job_status

	if [ $restn == 1 ]; then
	echo "Stage $restn equilibration of $protein 
 &cntrl
  imin = 0, nstlim = $ensteps, dt = $ets,
  irest = 0, ntx = 1,ig = -1,
  temp0 = $etemp,
  ntc = 2, ntf = 2, tol = 0.00001,
  ntwx = $ewfrq, ntwe = $ewfrq, ntwr = $ewrst, ntpr = $ewfrq,
  cut = $cutoff, iwrap = 0, igb = 8,           
  ntt = 3, gamma_ln = 1.0, ntb = 0,                       ! ntp (1 = isotropic position scaling)
  iphmd = 1, solvph = $crysph, saltcon = $conc,
  ntr = 1, restraintmask = ':1-${nres}&!@H=', restraint_wt = ${erestraints[$(($restn-1))]},
  ioutfm = 1, ntxo = 2,                        ! Fomrat of coor. and vel. trj files, write NetCDF restrt fuile for final coor., vel., and box size
/" > ${protein}_equil${restn}.mdin
	equilstart="${protein}_mini.rst7"
	$pmemddir -O -i ${protein}_equil${restn}.mdin -c $equilstart -p ${protein}.parm7 -ref ${protein}.rst7 -r ${protein}_equil${restn}.rst7 -o ${protein}_equil${restn}.out -x ${protein}_equil${restn}.nc -phmdin ${protein}_phmdin -phmdparm $inputparm -phmdout ${protein}_equil${restn}.lambda -phmdrestrt ${protein}_equil${restn}.phmdrst
	wait
	else
	echo "Stage $restn equilibration of $protein 
 &cntrl
  imin = 0, nstlim = $ensteps, dt = $ets,
  irest = 1, ntx = 5,ig = -1,
  temp0 = $etemp,
  ntc = 2, ntf = 2, tol = 0.00001,
  ntwx = $ewfrq, ntwe = $ewfrq, ntwr = $ewrst, ntpr = $ewfrq,
  cut = $cutoff, iwrap = 0, igb = 8,           
  ntt = 3, gamma_ln = 1.0, ntb = 0,                       ! ntp (1 = isotropic position scaling)
  iphmd = 1, solvph = $crysph, saltcon = $conc,
  ntr = 1, restraintmask = ':1-${nres}&!@H=', restraint_wt = ${erestraints[$(($restn-1))]},
  ioutfm = 1, ntxo = 2,                        ! Fomrat of coor. and vel. trj files, write NetCDF restrt fuile for final coor., vel., and box size
/" > ${protein}_equil${restn}.mdin
	sed -i 's/QPHMDStart = .true.,/QPHMDStart = .false.,/g' ${protein}_phmdin
	prev=$(($restn-1))
	equilstart="${protein}_equil${prev}.rst7"
	phmdstart="${protein}_equil${prev}.phmdrst"
	sed -i 's/PHMDRST/PHMDSTRT/g' $phmdstart
    	$pmemddir -O -i ${protein}_equil${restn}.mdin -c $equilstart -p ${protein}.parm7 -ref ${protein}.rst7 -r ${protein}_equil${restn}.rst7 -o ${protein}_equil${restn}.out -x ${protein}_equil${restn}.nc -phmdin ${protein}_phmdin -phmdparm $inputparm -phmdstrt $phmdstart -phmdout ${protein}_equil${restn}.lambda -phmdrestrt ${protein}_equil${restn}.phmdrst
	fi
    	echo 'equilibration-end' > ${currentdir}/job_status
    done
    #############################
    #  Perform Production #
    #############################
    
    # Create input files for Production 
    final_equil_stage=${#erestraints[@]}   
    sed -i 's/PHMDRST/PHMDSTRT/g' ${protein}_equil${final_equil_stage}.phmdrst 
    # Run Production
    echo 'production-begin' > ${currentdir}/job_status
    for pH in "${pHs[@]}"
    do
	echo "Production stage of $protein
 &cntrl
  imin = 0, nstlim = $pnsteps, dt = $pts, 
  irest = 1, ntx = 5, ig = -1, 
  tempi = $etemp, temp0 = $etemp, 
  ntc = 2, ntf = 2, tol = 0.00001,
  ntwx = $pwfrq, ntwe = $pwfrq, ntwr = $pwfrq, ntpr = $pwfrq, 
  cut = $cutoff, iwrap = 0, igb = 8,
  ntt = 3, gamma_ln = 1.0, ntb = 0,
  iphmd = 1, solvph= $pH, saltcon = $conc,
  ioutfm = 1, ntxo = 2,                        ! Fomrat of coor. and vel. trj files, write NetCDF restrt fuile for final coor., vel., and box size
/" > ${protein}_pH${pH}.mdin
    #$pmemddir -O -i ${protein}_pH${pH}.mdin -c ${protein}_equil${final_equil_stage}.rst7 -p ${protein}.parm7 -r ${protein}_pH${pH}_prod1.rst7 -o ${protein}_pH${pH}_prod1.out -x ${protein}_pH${pH}_prod1.nc -phmdin ${protein}_phmdin -phmdparm $inputparm -phmdstrt ${protein}_equil${final_equil_stage}.phmdrst -phmdout ${protein}_pH${pH}_prod1.lambda -phmdrestrt ${protein}_pH${pH}_prod1.phmdrst
    wait
    echo 'production-end' > ${currentdir}/job_status
    done

    #############################
    #  Clean up files   #
    #############################
    if [ "$istest" = false ] ; then
       rm tmp1.pdb tmp2.pdb *_tmp.pdb tmp*.parm7 2> /dev/null
       rm tleap.log mden mdinfo leap.log prepare.log build.in ${protein}_mini.mdin ${protein}.rst7 ${protein}_mini.out  2> /dev/null
    fi
  
else

    ###############################
    #  Perform Production Restart #
    ###############################
    echo 'restart-begin' > ${currentdir}/job_status
    for pH in "${pHs[@]}"
    do
	lastrst7=`ls -t ${protein}_pH${pH}_prod*.rst7|head -1`
	prev_stage=$(echo $lastrst7 |sed -e 's/.*prod\(.*\).rst7/\1/')
	stage=$(($prev_stage + 1))
	sed -i 's/PHMDRST/PHMDSTRT/g' ${protein}_pH${pH}_prod${prev_stage}.phmdrst

    # Run production restart
    	$pmemddir -O -i ${protein}_pH${pH}.mdin -c ${protein}_pH${pH}_prod${prev_stage}.rst7 -p ${protein}.parm7 -r ${protein}_pH${pH}_prod${stage}.rst7 -o ${protein}_pH${pH}_prod${stage}.out -x ${protein}_pH${pH}_prod${stage}.nc -phmdin ${protein}_phmdin -phmdparm $inputparm -phmdstrt ${protein}_pH${pH}_prod${prev_stage}.phmdrst -phmdout ${protein}_pH${pH}_prod${stage}.lambda -phmdrestrt ${protein}_pH${pH}_prod${stage}.phmdrst
    done
    echo 'restart-end' > ${currentdir}/job_status
fi
cd $currentdir
#rm build.in *log *.pdb 2> /dev/null
echo 'Done' > job_status
exit
