--MD Software--

AMBER 18

--Analysis Library Versions--

Python 3.7.12 numpy 1.20.2

CPPTRAJ: Version 4.14.0

VMD: Version 1.9.1

--Contents of Project--

Here, all input files for running the equilibration and production can be found along with the scripts used for analysis and plotting the data for each figure in the paper including the SI.
